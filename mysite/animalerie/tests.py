from .models import Equipement, Animal
from .views import nourrir, divertir, reveiller, coucher
from django.test import TestCase

class AnimalnModelTests(TestCase):
    def setUp(self):
        q = Equipement(lieu="NI", disponibilite=False)
        q.save()
        q.animal_set.create(animal="Pocahontas", race="opossum", type="marsupial", etat="EN")
        q = Equipement(lieu="LI", disponibilite=True)
        q.save()
        q.animal_set.create(animal="Tic", race="tamia", type="rongeur", etat="AF")
        q.animal_set.create(animal="Tac", race="tamia", type="rongeur", etat="AF")
        q.animal_set.create(animal="Patrick", race="hamster", type="rongeur", etat="AF")
        q = Equipement(lieu="MA", disponibilite=False)
        q.save()
        q.animal_set.create(animal="Totoro", race="ili pika", type="rongeur", etat="RE")
        q = Equipement(lieu="RO", disponibilite=True)
        q.save()

    def test_lit_etat(self):
        self.assertEqual(Animal.lit_etat('Tac'), 'AF')
        self.assertEqual(Animal.lit_etat('Bob'), None)

    def test_lit_lieu(self):
        self.assertEqual(Animal.lit_lieu('Tac'), 'LI')
        self.assertEqual(Animal.lit_lieu('Bob'), None)

    def test_change_etat(self):
        Animal.change_etat('Totoro', 'FA')
        self.assertEquals(Animal.lit_etat('Totoro'), "FA")
        Animal.change_etat('Totoro', 'excité comme un pou')
        self.assertEquals(Animal.lit_etat('Totoro'), "FA")

    def test_change_lieu(self):
        Animal.change_lieu('Totoro', 'RO')
        self.assertEquals(Animal.lit_lieu('Totoro'), "RO")
        Animal.change_lieu('Totoro', 'NI')
        self.assertEquals(Animal.lit_lieu('Totoro'), "RO")
        Animal.change_lieu('Totoro', 'nintendo')
        self.assertEquals(Animal.lit_lieu('Totoro'), "RO")
        Animal.change_lieu('Patrick', 'RO')
        self.assertEquals(Animal.lit_lieu('Patrick'), "LI")
        Animal.change_lieu('Patrick', 'MA')
        self.assertEquals(Animal.lit_lieu('Patrick'), "MA")
        Animal.change_lieu('Patrick', 'LI')
        self.assertEquals(Animal.lit_lieu('Patrick'), "LI")
        Animal.change_lieu('Totoro', 'LI')
        self.assertEquals(Animal.lit_lieu('Totoro'), "LI")
        Animal.change_lieu('Muche', 'LI')
        self.assertEquals(Animal.lit_lieu('Muche'), None)


class EquipmentModelTests(TestCase):
    def setUp(self):
        q = Equipement(lieu="NI", disponibilite=False)
        q.save()
        q.animal_set.create(animal="Pocahontas", race="opossum", type="marsupial", etat="EN")
        q = Equipement(lieu="LI", disponibilite=True)
        q.save()
        q.animal_set.create(animal="Tic", race="tamia", type="rongeur", etat="AF")
        q.animal_set.create(animal="Tac", race="tamia", type="rongeur", etat="AF")
        q.animal_set.create(animal="Patrick", race="hamster", type="rongeur", etat="AF")
        q = Equipement(lieu="MA", disponibilite=False)
        q.save()
        q.animal_set.create(animal="Totoro", race="ili pika", type="rongeur", etat="RE")
        q = Equipement(lieu="RO", disponibilite=True)
        q.save()

    def test_verifie_disponibilite(self):
        self.assertEquals(Equipement.verifie_disponibilite('LI'), True)
        self.assertEquals(Equipement.verifie_disponibilite('MA'), False)
        self.assertEquals(Equipement.verifie_disponibilite('nintendo'), None)

    def test_cherche_occupant(self):
        self.assertEquals(Equipement.cherche_occupant('MA'), [Animal.objects.get(animal='Totoro')])
        self.assertTrue(Animal.objects.get(animal='Tac') in Equipement.cherche_occupant('LI'))
        self.assertTrue(Animal.objects.get(animal='Tac') not in Equipement.cherche_occupant('RO'))
        self.assertEquals(Equipement.cherche_occupant('nintendo'), None)


class ControllerTests(TestCase):
    def setUp(self):
        q = Equipement(lieu="NI", disponibilite=False)
        q.animal_set.create(animal="Pocahontas", race="opossum", type="marsupial", etat="EN")
        q.save()
        q = Equipement(lieu="LI", disponibilite=True)
        q.save()
        q.animal_set.create(animal="Tic", race="tamia", type="rongeur", etat="AF")
        q.animal_set.create(animal="Tac", race="tamia", type="rongeur", etat="AF")
        q.animal_set.create(animal="Patrick", race="hamster", type="rongeur", etat="AF")
        q = Equipement(lieu="MA", disponibilite=False)
        q.save()
        q.animal_set.create(animal="Totoro", race="ili pika", type="rongeur", etat="RE")
        q = Equipement(lieu="RO", disponibilite=True)
        q.save()

    def test_nourrir(self):
        Animal.change_etat('Totoro', 'FA')
        Animal.change_lieu('Totoro', 'RO')
        if Equipement.verifie_disponibilite('MA') is True and Animal.lit_etat('Tic') == 'AF':
            nourrir('Tic')
        self.assertEquals(Equipement.verifie_disponibilite('MA'), False)
        self.assertEquals(Animal.lit_etat('Tic'), 'RE')
        self.assertEquals(Animal.lit_lieu('Tic'), 'MA')
        nourrir('Pocahontas')
        self.assertEquals(Animal.lit_etat('Pocahontas'), 'EN')
        self.assertEquals(Animal.lit_lieu('Pocahontas'), 'NI')
        nourrir('Tac')
        self.assertEquals(Animal.lit_etat('Tac'), 'AF')
        self.assertEquals(Animal.lit_lieu('Tac'), 'LI')
        nourrir('Bob')
        self.assertEquals(Animal.lit_etat('Bob'), None)
        self.assertEquals(Animal.lit_lieu('Bob'), None)
        self.assertEquals(Equipement.verifie_disponibilite('MA'), False)


    def test_divertir(self):
        divertir('Totoro')
        self.assertEqual(False, Equipement.verifie_disponibilite('RO'))
        self.assertEqual(True, Equipement.verifie_disponibilite('MA'))
        self.assertEqual('FA', Animal.lit_etat('Totoro'))
        self.assertEqual('RO', Animal.lit_lieu('Totoro'))
        Animal.change_etat('Tac', 'RE')
        Animal.change_lieu('Tac', 'MA')
        divertir('Tac')
        self.assertEqual('RE', Animal.lit_etat('Tac'))
        self.assertEqual('MA', Animal.lit_lieu('Tac'))
        divertir('Pocahontas')
        self.assertEqual('EN', Animal.lit_etat('Pocahontas'))
        self.assertEqual('NI', Animal.lit_lieu('Pocahontas'))
        divertir('Bob')
        self.assertEqual(None, Animal.lit_etat('Bob'))
        self.assertEqual(None, Animal.lit_lieu('Bob'))
        self.assertEqual(False, Equipement.verifie_disponibilite('RO'))

    def test_coucher_eins(self):
        Animal.change_etat('Totoro', 'FA')
        Animal.change_lieu('Totoro', 'RO')
        Animal.change_etat('Pocahontas', 'AF')
        Animal.change_lieu('Pocahontas', 'LI')
        coucher('Totoro')
        self.assertEqual(False, Equipement.verifie_disponibilite('NI'))
        self.assertEqual(True, Equipement.verifie_disponibilite('RO'))
        self.assertEqual('EN', Animal.lit_etat('Totoro'))
        self.assertEqual('NI', Animal.lit_lieu('Totoro'))

    def test_coucher_zwei(self):
        Animal.change_etat('Totoro', 'FA')
        Animal.change_lieu('Totoro', 'RO')
        coucher('Totoro')
        self.assertEqual('FA', Animal.lit_etat('Totoro'))
        self.assertEqual('RO', Animal.lit_lieu('Totoro'))
        coucher('Pocahontas')
        self.assertEqual('EN', Animal.lit_etat('Pocahontas'))
        self.assertEqual('NI', Animal.lit_lieu('Pocahontas'))
        coucher('Bob')
        self.assertEqual(None, Animal.lit_etat('Bob'))
        self.assertEqual(None, Animal.lit_lieu('Bob'))
        self.assertEqual(False, Equipement.verifie_disponibilite('RO'))


    def test_reveiller(self):
        reveiller('Pocahontas')
        self.assertEqual(True, Equipement.verifie_disponibilite('NI'))
        self.assertEqual('AF', Animal.lit_etat('Pocahontas'))
        self.assertEqual('LI', Animal.lit_lieu('Pocahontas'))
        reveiller('Totoro')
        self.assertEqual('RE', Animal.lit_etat('Totoro'))
        self.assertEqual('MA', Animal.lit_lieu('Totoro'))
        reveiller('Bob')
        self.assertEqual(None, Animal.lit_etat('Bob'))
        self.assertEqual(None, Animal.lit_lieu('Bob'))
        self.assertEqual(True, Equipement.verifie_disponibilite('LI'))

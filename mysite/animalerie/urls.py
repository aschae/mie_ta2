from django.urls import path

from . import views

app_name = 'animalerie'
urlpatterns = [
    path('', views.index, name='index'),
    path('controller/', views.controller, name='controller')
]
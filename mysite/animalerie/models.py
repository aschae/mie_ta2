from django.db import models

# Create your models here.

class Animal(models.Model):
    AFFAME = 'AF'
    FATIGUE = 'FA'
    REPUS = 'RE'
    ENDORMI = 'EN'
    ETAT_CHOICES = (
        (AFFAME, 'affam\u00e9'),
        (FATIGUE, 'fatigu\u00e9'),
        (REPUS, 'repus'),
        (ENDORMI, 'endormi'),
    )
    NID = 'NI'
    MANGEOIRE = 'MA'
    LITIERE = 'LI'
    ROUE = 'RO'
    LIEU_CHOICES = (
        (NID, 'nid'),
        (MANGEOIRE, 'mangeoire'),
        (LITIERE, 'liti\u00e8re'),
        (ROUE, 'roue'),
    )

    animal = models.CharField(max_length=100, primary_key=True)
    race = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    etat = models.CharField(
        max_length=2,
        choices=ETAT_CHOICES,
        default=AFFAME,
    )
    lieu = models.ForeignKey(
        'Equipement',
        on_delete=models.CASCADE,
    )

    @staticmethod
    def lit_etat(name):
        try:
            anim = Animal.objects.get(animal=name)
        except Animal.DoesNotExist:
            return None
        return anim.etat

    @staticmethod
    def lit_lieu(name):
        try:
            anim = Animal.objects.get(animal=name)
        except Animal.DoesNotExist:
            return None
        return anim.lieu.lieu

    @staticmethod
    def change_etat(name, etat):
        try:
            animal = Animal.objects.get(animal=name)
        except Animal.DoesNotExist:
            return None
        for pair in animal.ETAT_CHOICES:
            if etat in pair:
                animal.etat = etat
                animal.save()

    @staticmethod
    def change_lieu(name, nlieu):
        try:
            anim = Animal.objects.get(animal=name)
        except Animal.DoesNotExist:
            return None
        try:
            new_lieu = Equipement.objects.get(lieu=nlieu)
        except Equipement.DoesNotExist:
            return None
        if new_lieu.disponibilite is True:
            old_lieu = anim.lieu
            anim.lieu = new_lieu
            if not anim.lieu.lieu == "LI":
                anim.lieu.disponibilite = False
            old_lieu.disponibilite = True
            old_lieu.save()
            anim.lieu.save()
            anim.save()

    def __str__(self):
        return self.animal


class Equipement(models.Model):
    NID = 'NI'
    MANGEOIRE = 'MA'
    LITIERE = 'LI'
    ROUE = 'RO'
    LIEU_CHOICES = (
        (NID, 'nid'),
        (MANGEOIRE, 'mangeoire'),
        (LITIERE, 'liti\u00e8re'),
        (ROUE, 'roue'),
    )

    lieu = models.CharField(
        primary_key=True,
        max_length=2,
        choices=LIEU_CHOICES,
    )
    disponibilite = models.BooleanField()

    @staticmethod
    def verifie_disponibilite(equip):
        try:
            e = Equipement.objects.get(lieu=equip)
        except Equipement.DoesNotExist:
            return None
        return e.disponibilite

    @staticmethod
    def cherche_occupant(equip):
        try:
            e = Equipement.objects.get(lieu=equip)
        except Equipement.DoesNotExist:
            return None
        e = e.animal_set.all()
        res = []
        for anim in e:
            res += [anim]
        return res

    def __str__(self):
        return self.get_lieu_display()

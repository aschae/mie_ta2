from .models import Animal, Equipement
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

# Create your views here.


def index(request):
    action_list = ['nourrir', 'divertir', 'coucher', 'réveiller']
    animal_list = Animal.objects.order_by('animal')
    context = {'animal_list': animal_list, 'action_list': action_list}
    return render(request, 'animalerie/index.html', context)


def controller(request):
    action_list = ['nourrir', 'divertir', 'coucher', 'réveiller']
    animal_list = Animal.objects.order_by('animal')
    animal_names = []
    for anim in animal_list:
        animal_names += [anim.animal]
    if request.POST.get('animal', False) is False or request.POST.get('action', False) is False:
        context = {'animal_list': animal_list, 'action_list': action_list, 'error_message': "You didn't select an "
                                                                                            "animal and an action.",}
        return render(request, 'animalerie/index.html', context)

    action = request.POST['action']
    name = request.POST['animal']

    if name not in animal_names or action not in action_list:
        context = {'animal_list': animal_list, 'action_list': action_list, 'error_message': "Received unknown data.", }
        return render(request, 'animalerie/index.html', context)

    if action == 'nourrir':
        message = nourrir(name)
    elif action == 'divertir':
        message = divertir(name)
    elif action == 'coucher':
        message = coucher(name)
    elif action == 'réveiller':
        message = reveiller(name)
    else:
        message = "APOCALYPSE!!!"

    if message is True:
        return HttpResponseRedirect(reverse('animalerie:index'))
    else:
        context = {'animal_list': animal_list, 'action_list': action_list, 'error_message': message, }
        return render(request, 'animalerie/index.html', context)


# here begins controller logic

def nourrir(id_animal):
    if Animal.lit_etat(id_animal) is not None:
        if Animal.lit_etat(id_animal) != 'AF':
            return 'Désolé, ' + id_animal + " n'a pas faim..."
        elif Equipement.verifie_disponibilite('MA') is False:
            occupant = Equipement.cherche_occupant('MA') # TODO: list with strings
            namestring = ""
            for ani in occupant:
                namestring += ani.animal
            return 'Impossible, la mangeoire est actuellement occupée par ' + namestring
        else:
            Animal.change_etat(id_animal, 'RE')
            Animal.change_lieu(id_animal, 'MA')
            return True
    else:
        return "Désolé, " + id_animal + " n'est pas un animal connu"


def divertir(id_animal):
    if Animal.lit_etat(id_animal) is not None:
        if Animal.lit_etat(id_animal) != 'RE':
            return 'Désolé, ' + id_animal + " n'est pas en état de faire du sport."
        elif Equipement.verifie_disponibilite('RO') is False:
            occupant = Equipement.cherche_occupant('RO')
            namestring = ""
            for ani in occupant:
                namestring += ani.animal
            return 'Impossible, la roue est actuellement occupée par ' + namestring
        else:
            Animal.change_etat(id_animal, 'FA')
            Animal.change_lieu(id_animal, 'RO')
            return True
    else:
        return "Désolé, " + id_animal + " n'est pas un animal connu"


def coucher(id_animal):
    if Animal.lit_etat(id_animal) is not None:
        if Animal.lit_etat(id_animal) != 'FA':
            return 'Désolé, ' + id_animal + " n'est pas fatigué."
        elif Equipement.verifie_disponibilite('NI') is False:
            occupant = Equipement.cherche_occupant('NI')
            namestring = ""
            for ani in occupant:
                namestring += ani.animal
            return 'Impossible, le nid est actuellement occupé par ' + namestring
        else:
            Animal.change_etat(id_animal, 'EN')
            Animal.change_lieu(id_animal, 'NI')
            return True
    else:
        return "Désolé, " + id_animal + " n'est pas un animal connu"


def reveiller(id_animal):
    if Animal.lit_etat(id_animal) is not None:
        if Animal.lit_etat(id_animal) != 'EN':
            return 'Désolé, ' + id_animal + " ne dort pas."
        else:
            Animal.change_etat(id_animal, 'AF')
            Animal.change_lieu(id_animal, 'LI')
            return True
    else:
        return "Désolé, " + id_animal + " n'est pas un animal connu"
